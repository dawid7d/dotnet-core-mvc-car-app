using Microsoft.EntityFrameworkCore;
using DotnetMvcDemoCars.Models;

namespace DotnetMvcDemoCars.Data
{
    public class MvcCarContext : DbContext
    {
        public MvcCarContext(DbContextOptions<MvcCarContext> options) : base(options)
        {            
        }

        public DbSet<Car> Car {get; set;}
    }
}