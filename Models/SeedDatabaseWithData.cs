using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using DotnetMvcDemoCars.Data;
using System;
using System.Linq;

namespace DotnetMvcDemoCars.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcCarContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcCarContext>>()))
            {

                if (context.Car.Any())
                {
                    return;   
                }

                context.Car.AddRange(
                    new Car
                    {
                        Name = "Audi A3",
                        doorsNumber = 5,
                        distanceTravelled = 140221,
                        ProduceDate = DateTime.Parse("2020-12-10")
                    },
                    new Car
                    {
                        Name = "Fiat Bravo",
                        doorsNumber = 3,
                        distanceTravelled = 240252,
                        ProduceDate = DateTime.Parse("2016-01-16")
                    },
                    new Car
                    {
                        Name = "Citroen C2",
                        doorsNumber = 3,
                        distanceTravelled = 340274,
                        ProduceDate = DateTime.Parse("2002-02-08")
                    },
                    new Car
                    {
                        Name = "Volkswagen Polo",
                        doorsNumber = 5,
                        distanceTravelled = 154714,
                        ProduceDate = DateTime.Parse("2015-05-11")
                    }
                );
                context.SaveChanges();
            }
        }
    }
}