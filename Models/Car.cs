using System;
using System.ComponentModel.DataAnnotations;

namespace DotnetMvcDemoCars.Models
{
    public class Car
    {
        [Key]
        public int Id {get; set;}
        
        [Required]
        public string Name { get; set; }

        [Display(Name = "Doors Number")]
        public int doorsNumber { get; set; }

        [Display(Name = "Distance Travelled")]
        public int distanceTravelled {get; set;}

        [Display(Name = "Produce Date")]
        [DataType(DataType.Date)]
        public DateTime ProduceDate { get; set; }
    }
}
